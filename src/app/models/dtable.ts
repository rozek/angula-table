export interface PeriodicElement {
  name: string;
  value: number;
  surname: string;
  entryDate: Date;
  daysSince: number;
}
