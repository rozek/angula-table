import { Component, OnInit, ViewChild} from '@angular/core';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { formatDate, DatePipe } from '@angular/common';

import * as moment from 'moment'
import {PeriodicElement} from '../../models/dtable'


// let czas = new Date().toLocaleString()
console.log(moment().format("dddd, MMMM Do YYYY"));
const momentDate = (moment().format("dddd, MMMM Do YYYY"));

const dates = [
  (new Date("Friday, January, 17, 2020")),
  (new Date("Tuesday, May, 14, 2019")),
  (new Date("Thursday, June, 06, 2019")),
  (new Date("Monday, February, 10, 2020")),
  (new Date("Friday, February, 14, 2020")),
  (new Date("Friday, February, 14, 2020")),
  (new Date("Friday, February, 14, 2020")),
  (new Date("Friday, February, 14, 2020")),
  (new Date("Friday, February, 14, 2020")),
  (new Date("Friday, February, 14, 2020"))
]

var a = new Date("Wednesday, January, 15, 2020");
var b = new Date();

var days = (dates.map(e=> Math.round(((e.getTime()) -(b.getTime() ))/ (1000 * 3600 * 24))));


console.log({...days});
const extra = ({...days});

const ELEMENT_DATA: PeriodicElement[] = [
  {name: 'Hydrogen', surname: 'Bella', value: 32.00,entryDate:new Date("Friday, January, 17, 2020") , daysSince: -(days[0])},
  {name: 'Helium', surname: 'Collins', value: 5,entryDate:new Date("Tuesday, May, 14, 2019") , daysSince: -(days[1])},
  {name: 'Lithium', surname: 'Axios', value: 12,entryDate:new Date("Thursday, June, 06, 2019") , daysSince:-(days[2])},
  {name: 'Beryllium', surname: 'Fredrica', value: 317,entryDate:new Date("Monday, February, 10, 2020") , daysSince: -(days[3])},
  {name: 'Boron', surname: 'Schneitz', value: 52,entryDate:new Date("Friday, February, 14, 2020") , daysSince: -(days[4])},
  {name: 'Carbon', surname: 'Ruge', value: 18,entryDate:new Date("Friday, February, 14, 2020") , daysSince: -(days[5])},
  {name: 'Nitrogen', surname: 'Schelling-biggs', value: 7,entryDate:new Date("Friday, February, 14, 2020") , daysSince: -(days[6])},
  {name: 'Oxygen', surname: 'Richmond', value: 10,entryDate:new Date("Friday, February, 14, 2020") , daysSince: -(days[7])},
  {name: 'Fluorine', surname: 'Weldt', value: 34,entryDate:new Date("Friday, February, 14, 2020") , daysSince: -(days[8])},
  {name: 'Neon', surname: 'Patkou', value: 0,entryDate:new Date("Friday, February, 14, 2020") , daysSince: -(days[9])},
];


console.log(ELEMENT_DATA[0].daysSince);

// let x= ELEMENT_DATA.map(e=> console.log((e.entryDate.getTime()) -(new Date().getTime())/ (1000 * 3600 * 24)))
// let y= console.log(new Date()/ (1000 * 3600 * 24))


@Component({
  selector: 'app-dtable',
  templateUrl: './dtable.component.html',
  styleUrls: ['./dtable.component.css'],
})
export class DTableComponent implements OnInit {

    displayedColumns: string[] = ['name', 'surname', 'value', 'entryDate', 'daysSince'];
    dataSource = new MatTableDataSource(ELEMENT_DATA);

    daysSince() {
      for (let i = 0; i < ELEMENT_DATA.length; i++) {
        const element = ELEMENT_DATA[i]
        console.log(element.daysSince);

        return element.daysSince
      }
      }

    @ViewChild(MatSort, {static: true}) sort: MatSort;


  constructor() { }

  ngOnInit() {

    var b = new Date();
    // var days =  Math.round(((a.getTime())-(b.getTime()))/ (1000 * 3600 * 24))
    // var days =  Math.round(((dates.map(e=>getTime(e)))-(b.getTime()))/ (1000 * 3600 * 24))
     var days = (dates.map(e=> Math.round(((e.getTime()) -(b.getTime() ))/ (1000 * 3600 * 24))   ));

    this.dataSource.sort = this.sort;
    // console.log(fullDate());
    this.daysSince()
  }

}
